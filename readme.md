# Desafio Stocks Toro - Arthur Santos

##### Principais bibliotecas/frameworks usados no projeto
- NextJS(ReactJS) - Framework base do projeto - https://github.com/zeit/next.js
- Styled-Componentes - Usado para todos os estilos - https://github.com/styled-components/styled-components
- Rematch JS - Usado para o gerenciamento de estado (Redux) - https://github.com/rematch/rematch
- Jest / Enzyme - Usado para os testes da aplicação - https://github.com/airbnb/enzyme

##### Estrutura
O projeto está estruturado dentro da pasta `/src` da seguinte forma:
`/app` - Configurações da aplicação
`/assets` - Assets globais da aplicação ( css global )
`/components` - Componentes da aplicação
`/models` - Modelos da aplicação (Gerenciamento de estado / Actions / Reducers )
`/pages` - Páginas da aplicação ( Só tem a index )
`/static` - Arquivos estáticos da aplicação ( Imagens )
`/templates` - Templates ( Tem o template default )

##### Instruções
Usei o yarn para o gerenciamento de dependências, para iniciar o projeto é simples:
- Instale as dependências
```sh
$ yarn install
```
- Para rodar a aplicação em **dev**
```sh
$ yarn dev
```
- Para rodar a aplicação em **prod**
```sh
$ yarn build
$ yarn start
```
- Para rodar os **testes** da aplicação
```sh
$ yarn test
```
- Para rodar o **lint**
```sh
$ yarn lint
```

##### Observações
Consegui fazer o fluxo inteiro e usei o **JSON** que foi passado porém o link das imagens está quebrado, coloquei um background cinza como "emptystate".
