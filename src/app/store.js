import { init } from '@rematch/core';
import { prices, modal, login } from '../models';

export const initStore = (initialState = {}) => init({
    models: {
        prices,
        modal,
        login,
    },
    redux: {
        initialState,
    },
});
