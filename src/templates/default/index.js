import React from 'react';

import { initStore } from '../../app/store';
import withRematch from '../../app/withRematch';

import { GlobalStyle } from '../../assets/css/global';
import ModalC from '../../components/Modal';
import LoginC from '../../components/Login';

const templateDefault = (props) => {
    const { children } = props;
    return (
        <React.Fragment>
            <GlobalStyle />
            <ModalC />
            <LoginC />
            {children}
        </React.Fragment>
    );
};

export default withRematch(initStore, null, null)(templateDefault);
