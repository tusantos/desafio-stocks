import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import 'jest-enzyme';
import 'jest-styled-components';

import TemplateDefaultComp from '.';

describe('TemplateDefaultComp Test', () => {
    it('TemplateDefaultComp MatchSnapshot', () => {
        const component = <TemplateDefaultComp />;
        const tree = renderer.create(component).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('TemplateDefaultComp', () => {
        const component = mount(<TemplateDefaultComp />);
        expect(component.find('p')).toHaveLength(1);
        expect(component.find('section')).toHaveLength(6);
    });
});
