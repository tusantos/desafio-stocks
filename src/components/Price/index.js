import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
    Price,
    PriceHeader,
    Infos,
    Thumb,
    Company,
    PriceName,
    ButtonDetails,
    ButtonDetailsIcon,
    Details,
    DetailsText,
    DetailsTextOpen,
    DetailsTextNow,
    DetailsActionButton,
} from './style';

export class PriceComp extends React.Component {
    static defaultProps = {
        logo: '',
        companyName: 'Sem nome',
        priceName: 'Sem nome',
        actualValue: '0.01',
        openingValue: '0.02',
    };

    static propTypes = {
        logo: PropTypes.string,
        companyName: PropTypes.string,
        priceName: PropTypes.string,
        actualValue: PropTypes.string,
        openingValue: PropTypes.string,
    };

    state = {
        details: false,
    };

    toggleDetails = () => {
        const { details } = this.state;
        this.setState({
            details: !details,
        });
    };

    setTextButton = (openingValue, actualValue) => {
        if (actualValue > openingValue) {
            return 'Comprar';
        }
        return 'Vender';
    };

    openModal = (openingValue, actualValue) => {
        const { changeShowModal, changeTextModal } = this.props;
        const text = actualValue > openingValue
            ? 'Tem certeza que deseja comprar essa ação?'
            : 'Tem certeza que deseja vender essa ação?';
        changeShowModal(true);
        changeTextModal(text);
    };

    render() {
        const { details } = this.state;
        const {
            logo, companyName, priceName, actualValue, openingValue,
        } = this.props;
        return (
            <Price>
                <PriceHeader details={details}>
                    <Infos>
                        <Thumb url={logo} />
                        <Company>
                            {companyName}
                            <PriceName>
                                {' '}
                                -
                                {' '}
                                {priceName}
                            </PriceName>
                        </Company>
                    </Infos>
                    <ButtonDetails onClick={() => this.toggleDetails()}>
                        {details ? 'Ocultar detalhe da ação' : 'Ver detalhe da ação'}
                        {' '}
                        <ButtonDetailsIcon details={details} />
                    </ButtonDetails>
                </PriceHeader>
                <Details details={details}>
                    <Infos>
                        <DetailsText>
                            <DetailsTextOpen>Abertura:</DetailsTextOpen>
                            {' '}
                            {openingValue}
                        </DetailsText>
                        <DetailsText>
                            <DetailsTextNow>Atual:</DetailsTextNow>
                            {' '}
                            {actualValue}
                        </DetailsText>
                    </Infos>
                    <DetailsActionButton onClick={() => this.openModal(openingValue, actualValue)}>
                        {this.setTextButton(openingValue, actualValue)}
                    </DetailsActionButton>
                </Details>
            </Price>
        );
    }
}

const mapDispatch = ({
    modal: { changeShowModal, changeTextModal },
}) => ({
    changeShowModal,
    changeTextModal,
});

export default connect(
    null,
    mapDispatch,
)(PriceComp);
