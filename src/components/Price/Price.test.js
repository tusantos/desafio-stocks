import { mount } from 'enzyme';

import renderer from 'react-test-renderer';
import 'jest-enzyme';
import 'jest-styled-components';

import { PriceComp } from '.';

const fakePropsVenda = {
    logo: 'https://s29.postimg.org/lnt4mtp2f/petrobras.png',
    companyName: 'Petrobras',
    priceName: 'PETR4',
    actualValue: '28.05',
    openingValue: '28.10',
};
const fakePropsCompra = {
    logo: 'https://s29.postimg.org/lnt4mtp2f/petrobras.png',
    companyName: 'Petrobras2',
    priceName: 'PETR42',
    actualValue: '28.15',
    openingValue: '28.10',
};

describe('PriceComp Test', () => {
    it('PriceComp MatchSnapshot', () => {
        const component = <PriceComp />;
        const tree = renderer.create(component).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('PriceComp Venda', () => {
        const component = mount(<PriceComp {...fakePropsVenda} />);
        expect(component.find('p')).toHaveLength(2);
        expect(component.find('section')).toHaveLength(9);
        expect(component.prop('logo')).toBe(fakePropsVenda.logo);
        expect(component.prop('companyName')).toBe(fakePropsVenda.companyName);
        expect(component.prop('priceName')).toBe(fakePropsVenda.priceName);
        expect(component.prop('actualValue')).toBe(fakePropsVenda.actualValue);
        expect(component.prop('openingValue')).toBe(fakePropsVenda.openingValue);
        expect(component.find('section').last().text()).toBe('Vender');
    });
    it('PriceComp Compra', () => {
        const component = mount(<PriceComp {...fakePropsCompra} />);
        expect(component.find('section').last().text()).toBe('Comprar');
    });
});
