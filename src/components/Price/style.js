import styled from 'styled-components';

export const Price = styled.section`
    width: 100%;
    max-width: 830px;
    margin-bottom: 25px;
    &:last-child {
        margin-bottom: 0px;
    }
`;

export const PriceHeader = styled.section`
    width: 100%;
    height: 72px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    background: #ffffff;
    padding: 0px 26px;
    border-top: solid 1px #e3e3e3;
    border-left: solid 1px #e3e3e3;
    border-right: solid 1px #e3e3e3;
    border-bottom: solid 1px #e3e3e3;
    border-radius: ${(props) => {
        if (props.details) {
            return '3px 3px 0px 0px';
        }
        return '3px';
    }};
`;

export const Infos = styled.section`
    display: flex;
    align-items: center;
    height: 100%;
`;

export const Thumb = styled.section`
    width: 37px;
    height: 37px;
    background-image: ${(props) => {
        if (props.url) {
            return `url('${props.url}')`;
        }
        return '';
    }};
    background: #eaeaea;
    margin-right: 18px;
`;

export const Company = styled.section`
    color: #9a9a9a;
    font-size: 18px;
    font-weight: 500;
    text-transform: uppercase;
`;
export const PriceName = styled.span`
    color: #b8b8b8;
    font-size: 18px;
    font-weight: 500;
    text-transform: uppercase;
`;

export const ButtonDetails = styled.section`
    display: flex;
    align-items: center;
    font-size: 18px;
    font-weight: 500;
    color: #ba7fd8;
    cursor: pointer;
`;

export const ButtonDetailsIcon = styled.div`
    width: 15px;
    height: 9px;
    background: url('/static/img/buttonDetailsIcon.svg');
    margin-left: 10px;
    transition: all 0.3s;
    transform: ${(props) => {
        if (props.details) {
            return 'rotate(0deg)';
        }
        return 'rotate(180deg)';
    }};;
`;

export const Details = styled.section`
    width: 100%;
    padding: 0px 26px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    transition: all 0.3s;
    height: ${(props) => {
        if (props.details) {
            return '112px';
        }
        return '0px';
    }};
    overflow: hidden;
    background: #fff;
    border-left: solid 1px #e3e3e3;
    border-right: solid 1px #e3e3e3;
    border-bottom: ${(props) => {
        if (props.details) {
            return 'solid 1px #e3e3e3;';
        }
        return 'none';
    }};
    border-radius: 0px 0px 3px 3px;
`;

export const DetailsText = styled.p`
    font-size: 18px;
    font-weight: 500;
    color: #9a9a9a;
`;

export const DetailsTextOpen = styled.span`
    color: #2ecc71;
`;

export const DetailsTextNow = styled.span`
    color: #ff8e8e;
    margin-left: 60px;
`;

export const DetailsActionButton = styled.section`
    width: 100%;
    max-width: 154px;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 3px;
    background: #f3d36f;
    font-size: 20px;
    font-weight: 700;
    text-transform: uppercase;
    color: #ffffff;
    cursor: pointer;
`;
