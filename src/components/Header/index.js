import { HeaderStocks, LogoStocks } from './style';

export default (props) => {
    const { children } = props;
    return (
        <HeaderStocks>
            <LogoStocks>stocks.</LogoStocks>
            {children}
        </HeaderStocks>
    );
};
