import styled from 'styled-components';

export const HeaderStocks = styled.header`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
    min-height: 455px;
    background: url('/static/img/bgHeaderStocks.svg') no-repeat;
    background-size: cover;
    background-position: 0% 100%;
`;

export const LogoStocks = styled.section`
    font-family: 'Montserrat', sans-serif;
    color: #ffffff;
    font-size: 45px;
    letter-spacing: -4.6px;
    font-weight: 700;
    font-style: normal;
    margin-top: 80px;
`;
