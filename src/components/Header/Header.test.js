import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import 'jest-enzyme';
import 'jest-styled-components';

import HeaderComp from '.';

describe('HeaderComp Test', () => {
    it('HeaderComp MatchSnapshot', () => {
        const component = <HeaderComp />;
        const tree = renderer.create(component).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('HeaderComp', () => {
        const component = mount(<HeaderComp />);
        expect(component.find('header')).toHaveLength(1);
        expect(component.find('section')).toHaveLength(1);
        expect(component.find('section').text()).toBe('stocks.');
    });
});
