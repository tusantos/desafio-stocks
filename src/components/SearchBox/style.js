import styled from 'styled-components';

export const SearchBox = styled.section`
    width: 100%;
    max-width: 690px;
    height: 72px;
    display: flex;
    border-radius: 3px;
    background: #fff;
    margin-top: 80px;
    box-shadow: 0 2px 9px 0 rgba(197, 197, 197, 0.5);
`;

export const SearchBoxInput = styled.input`
    width: 100%;
    max-width: 572px;
    background: none;
    color: #868686;
    border: none;
    border-radius: 3px 0px 0px 3px;
    height: 100%;
    font-size: 22px;
    font-weight: 500;
    padding: 23px 25px;
    &::placeholder {
        font-size: 22px;
        font-weight: 500;
        color: #b8b8b8;
    }
`;

export const ButtonSearch = styled.section`
    width: 100%;
    max-width: 118px;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 0px 3px 3px 0px;
    background: #f4db6f;
    cursor: pointer;
`;

export const SearchResultsBox = styled.section`
    width: 100%;
    max-width: 690px;
    height: auto;
    box-shadow: 0 2px 9px 0 rgba(197, 197, 197, 0.5);
    border: solid 1px #e7e7e7;
    background-color: #ffffff;
    margin-top: -1px;
    margin-bottom: 87px;
    padding: 0px 25px;
`;

export const SearchResult = styled.a`
    font-size: 20px;
    font-weight: 700;
    width: 100%;
    display: block;
    padding: 23px 0px;
    color: #a7a7a7;
    cursor: pointer;
`;
