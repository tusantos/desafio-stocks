import { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import {
    SearchBox, SearchBoxInput, ButtonSearch, SearchResultsBox, SearchResult,
} from './style';

export class SearchBoxComp extends Component {
    state = {
        placeholder: 'Pesquisar',
    };

    searchPrice = (e) => {
        const { findPrice } = this.props;
        findPrice(e.target.value);
    };

    setCompany = (priceName) => {
        const priceNameFind = priceName || this.inputSearchBoxInput.value;
        if (priceNameFind.length > 1) {
            const { findCompany, findPrice } = this.props;
            this.inputSearchBoxInput.value = '';
            this.setState({
                placeholder: `Termo pesquisado: ${priceNameFind}`,
            });
            findCompany(priceNameFind);
            findPrice('');
        }
    };

    enterPress = (event) => {
        if (event.key === 'Enter') {
            this.setCompany();
        }
    }

    render() {
        const { searchResult } = this.props;
        const { placeholder } = this.state;
        return (
            <Fragment>
                <SearchBox>
                    <SearchBoxInput
                      placeholder={placeholder}
                      ref={(el) => { this.inputSearchBoxInput = el; }}
                      onChange={e => this.searchPrice(e)}
                      onKeyPress={e => this.enterPress(e)}
                    />
                    <ButtonSearch onClick={() => this.setCompany()}>
                        <img src="/static/img/lupaButtonSearch.svg" alt="Lupa Pesquisar" />
                    </ButtonSearch>
                </SearchBox>
                {searchResult && searchResult.length > 0 && (
                    <SearchResultsBox>
                        {searchResult.map(value => (
                            <SearchResult
                              onClick={() => this.setCompany(value.name)}
                              key={value.name}
                            >
                                {value.name}
                            </SearchResult>
                        ))}
                    </SearchResultsBox>
                )}
            </Fragment>
        );
    }
}

const mapState = state => ({
    searchResult: state.prices.searchResult,
});

const mapDispatch = ({ prices: { findPrice, findCompany } }) => ({
    findPrice,
    findCompany,
});


export default connect(
    mapState,
    mapDispatch,
)(SearchBoxComp);
