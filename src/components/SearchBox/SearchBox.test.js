import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import 'jest-enzyme';
import 'jest-styled-components';

import { SearchBoxComp } from '.';

describe('SearchBoxComp Test', () => {
    it('SearchBoxComp MatchSnapshot', () => {
        const component = <SearchBoxComp />;
        const tree = renderer.create(component).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('SearchBoxComp', () => {
        const component = mount(<SearchBoxComp />);
        expect(component.find('input')).toHaveLength(1);
        expect(component.find('section')).toHaveLength(2);
        expect(component.find('img')).toHaveLength(1);
        expect(component.find('img').html()).toBe('<img src="/static/img/lupaButtonSearch.svg" alt="Lupa Pesquisar">');
    });
});
