import { mount } from 'enzyme';

import renderer from 'react-test-renderer';
import 'jest-enzyme';
import 'jest-styled-components';

import { SearchResultComp } from '.';

describe('SearchResultComp Test', () => {
    it('SearchResultComp MatchSnapshot', () => {
        const component = <SearchResultComp />;
        const tree = renderer.create(component).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('SearchResultComp', () => {
        const component = mount(<SearchResultComp />);
        expect(component.find('p')).toHaveLength(1);
        expect(component.find('section')).toHaveLength(3);
    });
});
