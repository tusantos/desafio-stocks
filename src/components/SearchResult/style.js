import styled from 'styled-components';

export const SearchResult = styled.section`
    width: 100%;
    max-width: 830px;
    margin: 0 auto;
    margin-top: 50px;
    margin-bottom: 50px;
`;

export const SearchResultText = styled.p`
    font-size: 18px;
    font-weight: 700;
    letter-spacing: 0.5px;
    color: #939393;
    margin-bottom: 20px;
`;
