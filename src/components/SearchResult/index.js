import { connect } from 'react-redux';

import { SearchResult, SearchResultText } from './style';
import SearchEmptyState from '../SearchEmptyState';
import Price from '../Price';

export const SearchResultComp = (props) => {
    const { company, searchOneTime } = props;
    return (
        <SearchResult>
            {company && <SearchResultText>Resultado da busca</SearchResultText>}
            {company
                && company.stock.map(value => (
                    <Price
                      key={value.name}
                      logo={company.logo}
                      companyName={company.name}
                      priceName={value.name}
                      actualValue={value.actualvalue}
                      openingValue={value.openingvalue}
                    />
                ))}
            {!company && !searchOneTime && (
                <SearchEmptyState text="Você ainda não realizou nenhuma busca!" />
            )}
            {!company && searchOneTime && (
                <SearchEmptyState text="Não encontramos nenhum resultado!" />
            )}
        </SearchResult>
    );
};

const mapState = state => ({
    company: state.prices.company,
    searchOneTime: state.prices.searchOneTime,
});

export default connect(
    mapState,
    null,
)(SearchResultComp);
