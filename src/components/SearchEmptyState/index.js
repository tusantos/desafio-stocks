import PropTypes from 'prop-types';

import { SearchEmptyState, IconEmptyState, TextEmptyState } from './style';

const SearchEmptyStateComp = (props) => {
    const { text } = props;
    return (
        <SearchEmptyState>
            <IconEmptyState />
            <TextEmptyState>{text}</TextEmptyState>
        </SearchEmptyState>
    );
};

SearchEmptyStateComp.defaultProps = {
    text: 'Você ainda não realizou nenhuma busca!',
};

SearchEmptyStateComp.propTypes = {
    text: PropTypes.string,
};

export default SearchEmptyStateComp;
