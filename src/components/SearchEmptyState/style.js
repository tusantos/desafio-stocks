import styled from 'styled-components';

export const SearchEmptyState = styled.section`
    width: 100%;
    max-width: 400px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin: 0 auto;
    margin-top: 60px;
`;

export const IconEmptyState = styled.section`
    width: 166px;
    height: 165px;
    background: url('/static/img/searchEmptyStateIcon.svg') no-repeat;
`;

export const TextEmptyState = styled.p`
    width: 100%;
    max-width: 220px;
    font-size: 20px;
    font-weight: 500;
    line-height: 1.6;
    color: #c8c8c8;
    text-align: center;
    margin-top: 24px;
`;
