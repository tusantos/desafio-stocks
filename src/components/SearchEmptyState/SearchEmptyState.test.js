import { mount } from 'enzyme';

import renderer from 'react-test-renderer';
import 'jest-enzyme';
import 'jest-styled-components';

import SearchEmptyStateComp from '.';
import { IconEmptyState } from './style';

describe('SearchEmptyStateComp Test', () => {
    it('SearchEmptyStateComp MatchSnapshot', () => {
        const component = <SearchEmptyStateComp />;
        const tree = renderer.create(component).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('SearchEmptyStateComp', () => {
        const component = mount(<SearchEmptyStateComp />);
        expect(component.find('p')).toHaveLength(1);
        expect(component.find('section')).toHaveLength(2);
        expect(component.find('p').text()).toBe('Você ainda não realizou nenhuma busca!');
    });
    it('IconEmptyState', () => {
        const component = mount(<IconEmptyState />);
        expect(component).toHaveStyleRule('background', "url('/static/img/searchEmptyStateIcon.svg') no-repeat");
    });
});
