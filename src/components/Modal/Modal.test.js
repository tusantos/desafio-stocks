import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import 'jest-enzyme';
import 'jest-styled-components';

import { ModalComp } from '.';

const fakeProps = {
    show: true,
    text: 'texto fake',
};

describe('ModalComp Test', () => {
    it('ModalComp MatchSnapshot', () => {
        const component = <ModalComp />;
        const tree = renderer.create(component).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('ModalComp', () => {
        const component = mount(<ModalComp {...fakeProps} />);
        expect(component.find('p')).toHaveLength(0);
        expect(component.find('h2')).toHaveLength(1);
        expect(component.find('section')).toHaveLength(3);
        expect(component.find('a')).toHaveLength(2);
        expect(component.state().show).toBe(false);
        expect(component.state().text).toBe('');
        expect(component.find('a').first().text()).toBe('Não');
        expect(component.find('a').last().text()).toBe('Sim');
    });
});
