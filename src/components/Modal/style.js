import styled from 'styled-components';

export const ModalContainer = styled.section`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    background-color: rgba(0,0,0,0.78);
    transition: all 0.2s;
    opacity: ${(props) => {
        if (props.show) {
            return 1;
        }
        return 0;
    }};
    visibility: ${(props) => {
        if (props.show) {
            return 'visibility';
        }
        return 'hidden';
    }};
    z-index: 100;
`;

export const Modal = styled.section`
    width: 100%;
    max-width: 447px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 185px;
    border-radius: 3px;
    background-color: #ffffff;
`;

export const ModalTitle = styled.h2`
    font-size: 18px;
    font-weight: 500;
    color: #606060;
`;

export const ModalOptions = styled.section`
    width: 100%;
    max-width: 160px;
    display: flex;
    justify-content: space-between;
    margin-top: 45px;
`;

export const ModalOption = styled.a`
    font-size: 18px;
    font-weight: 700;
    color: #ba7fd8;
    text-transform: uppercase;
    cursor: pointer;
`;
