import { Component } from 'react';
import { connect } from 'react-redux';

import {
    ModalContainer, Modal, ModalTitle, ModalOptions, ModalOption,
} from './style';

export class ModalComp extends Component {
    state = {
        show: false,
        text: '',
    };

    componentWillReceiveProps(nextProps) {
        if ('modal' in nextProps) {
            this.setState({
                show: nextProps.modal.show,
                text: nextProps.modal.text,
            });
        }
    }

    closeModal = () => {
        this.setState({
            show: false,
        });
    };

    loginForm = () => {
        const { changeShowLogin } = this.props;
        this.closeModal();
        changeShowLogin(true);
    }

    render() {
        const { show, text } = this.state;
        return (
            <ModalContainer show={show}>
                <Modal>
                    <ModalTitle>{text}</ModalTitle>
                    <ModalOptions>
                        <ModalOption onClick={() => this.closeModal()}>Não</ModalOption>
                        <ModalOption onClick={() => this.loginForm()}>Sim</ModalOption>
                    </ModalOptions>
                </Modal>
            </ModalContainer>
        );
    }
}

const mapState = state => ({
    modal: state.modal,
});

const mapDispatch = ({
    login: { changeShowLogin },
}) => ({
    changeShowLogin,
});

export default connect(
    mapState,
    mapDispatch,
)(ModalComp);
