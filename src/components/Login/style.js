import styled from 'styled-components';

export const LoginBlackContainer = styled.section`
    width: 100%;
    height: 100%;
    position: absolute;
    z-index: 100;
    background-color: rgba(0,0,0,0.78);
    transition: all 0.7s;
    opacity: ${(props) => {
        if (props.show) {
            return 1;
        }
        return 0;
    }};
    visibility: ${(props) => {
        if (props.show) {
            return 'visibility';
        }
        return 'hidden';
    }};
`;

export const LoginContainer = styled.section`
    width: 100%;
    max-width: 448px;
    height: 100%;
    position: absolute;
    right: 0px;
    z-index: 150;
    transition: all 0.7s;
    opacity: ${(props) => {
        if (props.show) {
            return 1;
        }
        return 0;
    }};
    visibility: ${(props) => {
        if (props.show) {
            return 'visibility';
        }
        return 'hidden';
    }};
`;
export const Login = styled.section`
    width: 100%;
    max-width: 448px;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    background: #ffffff;
    p {
        width: 100%;
        max-width: 370px;
        font-size: 18px;
        font-weight: 500;
        color: #6d6d6d;
        font-family: 'Montserrat', sans-serif;
        margin-top: 120px;
        margin-bottom: 32px;
        text-align: left;
    }
`;

export const LoginInput = styled.input`
    width: 100%;
    max-width: 370px;
    border-radius: 4px;
    border: solid 2px #c6c6c6;
    padding: 16px 24px;
    font-size: 16px;
    color: #a9a9a9;
    font-weight: 400;
    font-family: 'Montserrat', sans-serif;
    margin-bottom: 32px;
    &::placeholder {
        color: #a9a9a9;
        font-weight: 400;
    }
`;

export const LoginSubmit = styled.input`
    width: 100%;
    max-width: 370px;
    border: none;
    height: 54px;
    border-radius: 4px;
    background: #c545df;
    font-size: 18px;
    font-weight: 600;
    text-transform: uppercase;
    color: #fff;
    font-family: 'Montserrat', sans-serif;
`;
