import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import 'jest-enzyme';
import 'jest-styled-components';

import { LoginComp } from '.';

describe('LoginComp Test', () => {
    it('LoginComp MatchSnapshot', () => {
        const component = <LoginComp />;
        const tree = renderer.create(component).toJSON();
        expect(tree).toMatchSnapshot();
    });
    it('LoginComp', () => {
        const component = mount(<LoginComp />);
        expect(component.find('p')).toHaveLength(1);
        expect(component.find('input')).toHaveLength(3);
        expect(component.find('section')).toHaveLength(3);
        expect(component.state().show).toBe(false);
        expect(component.find('input').last().html()).toContain('type="submit"');
    });
});
