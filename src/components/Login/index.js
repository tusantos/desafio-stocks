import { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import {
    LoginBlackContainer, LoginContainer, Login, LoginInput, LoginSubmit,
} from './style';

export class LoginComp extends Component {
    state = {
        show: false,
    };

    componentWillReceiveProps(nextProps) {
        if ('showLogin' in nextProps) {
            this.setState({
                show: nextProps.showLogin,
            });
        }
    }

    render() {
        const { show } = this.state;
        const { changeShowLogin } = this.props;
        return (
            <Fragment>
                <LoginBlackContainer onClick={() => changeShowLogin(false)} show={show} />
                <LoginContainer show={show}>
                    <Login>
                        <p>Informe seu email e senha</p>
                        <LoginInput placeholder="Email" />
                        <LoginInput placeholder="Senha" />
                        <LoginSubmit type="submit" value="Login" />
                    </Login>
                </LoginContainer>
            </Fragment>
        );
    }
}

const mapState = state => ({
    showLogin: state.login.show,
});

const mapDispatch = ({
    login: { changeShowLogin },
}) => ({
    changeShowLogin,
});

export default connect(
    mapState,
    mapDispatch,
)(LoginComp);
