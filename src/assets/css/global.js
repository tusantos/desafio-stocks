import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        outline: 0;
    }
    html, body {
        height: 100%;
        font-family: 'Roboto', sans-serif;
    }
    body {
        text-rendering: optimizeLegibility !important;
        -webkit-font-smoothing: antialiased !important;
        background: #fafafa;
    }
    #__next {
        height: 100%;
        overflow: auto;
    }
`;
