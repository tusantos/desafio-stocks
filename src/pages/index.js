import React from 'react';

import DefaultTemplate from '../templates/default';
import Header from '../components/Header';
import SearchBox from '../components/SearchBox';
import SearchResult from '../components/SearchResult';

export default () => (
    <DefaultTemplate>
        <Header>
            <SearchBox />
        </Header>
        <SearchResult />
    </DefaultTemplate>
);
