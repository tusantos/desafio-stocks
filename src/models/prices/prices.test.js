import { init } from '@rematch/core';
import PriceModel from '.';

const store = init({
    models: { PriceModel },
});

const price = {
    name: 'VALE',
    logo: 'https://s29.postimg.org/u8mgdzz8n/vale.png',
    stock: [
        {
            name: 'VALE4',
            openingvalue: '17.65',
            actualvalue: '17.32',
        },
        {
            name: 'VALE3',
            openingvalue: '21.11',
            actualvalue: '21.32',
        },
    ],
};

describe('PriceModel test', () => {
    it('effect: findPricesInMock', async () => {
        const resp = await store.dispatch.PriceModel.findPricesInMock('val');
        expect(resp.indexCompany.logo).toBe(price.logo);
        expect(resp.indexCompany.name).toBe(price.name);
        expect(resp.indexCompany.stock[0].openingvalue).toBe(price.stock[0].openingvalue);
        expect(resp.indexCompany.stock[0].actualvalue).toBe(price.stock[0].actualvalue);
    });
    it('effect: findPrice', async () => {
        await store.dispatch.PriceModel.findPrice(price.name);

        const PriceModelState = store.getState().PriceModel;
        expect(PriceModelState.searchResult[0].name).toBe(price.stock[0].name);
        expect(PriceModelState.searchResult[0].openingvalue).toBe(price.stock[0].openingvalue);
        expect(PriceModelState.searchResult[0].actualvalue).toBe(price.stock[0].actualvalue);
    });
    it('effect: findCompany', async () => {
        await store.dispatch.PriceModel.findCompany(price.name);

        const PriceModelState = store.getState().PriceModel;
        expect(PriceModelState.company.name).toBe(price.name);
        expect(PriceModelState.company.logo).toBe(price.logo);
        expect(PriceModelState.company.stock[0].name).toBe(price.stock[0].name);
        expect(PriceModelState.company.stock[0].openingvalue).toBe(price.stock[0].openingvalue);
        expect(PriceModelState.company.stock[0].actualvalue).toBe(price.stock[0].actualvalue);
        expect(PriceModelState.searchOneTime).toBe(true);
    });

    it('reducer: setSearchResult', () => {
        store.dispatch.PriceModel.setSearchResult(price.stock);
        const PriceModelState = store.getState().PriceModel;
        expect(PriceModelState.searchResult).toBe(price.stock);
    });
    it('reducer: setCompany', () => {
        store.dispatch.PriceModel.setCompany(price);
        const PriceModelState = store.getState().PriceModel;
        expect(PriceModelState.company).toBe(price);
    });
    it('reducer: setSearchOneTime', () => {
        store.dispatch.PriceModel.setSearchOneTime(true);
        const PriceModelState = store.getState().PriceModel;
        expect(PriceModelState.searchOneTime).toBe(true);
    });
});
