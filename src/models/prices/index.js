import mockData from './stocksMock.json';

const prices = {
    state: {
        company: null,
        searchResult: null,
        searchOneTime: false,
    },
    reducers: {
        setCompany(state, payload) {
            return { ...state, company: payload };
        },
        setSearchResult(state, payload) {
            return { ...state, searchResult: payload };
        },
        setSearchOneTime(state, payload) {
            return { ...state, searchOneTime: payload };
        },
    },
    effects: {
        findPricesInMock(payload) {
            const data = mockData.market.company;
            const term = payload;
            let indexCompany = null;
            const priceValues = [];
            data.map((value, index) => value.stock.filter((value2) => {
                if (value2.name.toLowerCase().includes(term.toLowerCase())) {
                    indexCompany = index;
                    priceValues.push(value2);
                }
                return null;
            }));
            return {
                indexCompany: data[indexCompany],
                priceValues,
            };
        },
        async findPrice(payload) {
            if (payload.length > 1) {
                const finder = await this.findPricesInMock(payload);
                if (finder.priceValues.length > 0) {
                    this.setSearchResult(finder.priceValues);
                }
            }
            if (payload.length === 0) {
                this.setSearchResult([]);
            }
        },
        async findCompany(payload) {
            const finder = await this.findPricesInMock(payload);
            this.setSearchOneTime(true);
            if (finder.indexCompany) {
                this.setCompany(finder.indexCompany);
            } else {
                this.setCompany(null);
            }
        },
    },
};

export default prices;
