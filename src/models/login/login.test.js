import { init } from '@rematch/core';
import LoginModel from '.';

const store = init({
    models: { LoginModel },
});

describe('LoginModel test', () => {
    it('effect: changeShowLogin', async () => {
        await store.dispatch.LoginModel.changeShowLogin(true);

        const LoginModelState = store.getState().LoginModel;
        expect(LoginModelState.show).toBe(true);
    });

    it('reducer: setShow', () => {
        store.dispatch.LoginModel.setShow(true);
        const LoginModelState = store.getState().LoginModel;
        expect(LoginModelState.show).toBe(true);
    });
});
