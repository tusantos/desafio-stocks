const login = {
    state: {
        show: false,
    },
    reducers: {
        setShow(state, payload) {
            return { ...state, show: payload };
        },
    },
    effects: {
        changeShowLogin(showState) {
            this.setShow(showState);
        },
    },
};

export default login;
