const modal = {
    state: {
        show: false,
        text: null,
    },
    reducers: {
        setText(state, payload) {
            return { ...state, text: payload };
        },
        setShow(state, payload) {
            return { ...state, show: payload };
        },
    },
    effects: {
        changeTextModal(text) {
            this.setText(text);
        },
        changeShowModal(showState) {
            this.setShow(showState);
        },
    },
};

export default modal;
