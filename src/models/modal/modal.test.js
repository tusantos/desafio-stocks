import { init } from '@rematch/core';
import ModalModel from '.';

const store = init({
    models: { ModalModel },
});
const text = 'texto de teste';

describe('ModalModel test', () => {
    it('effect: changeShowModal', async () => {
        await store.dispatch.ModalModel.changeShowModal(true);

        const ModalModelState = store.getState().ModalModel;
        expect(ModalModelState.show).toBe(true);
    });
    it('effect: changeTextModal', async () => {
        await store.dispatch.ModalModel.changeTextModal(text);

        const ModalModelState = store.getState().ModalModel;
        expect(ModalModelState.text).toBe(text);
    });

    it('reducer: setShow', () => {
        store.dispatch.ModalModel.setShow(true);
        const ModalModelState = store.getState().ModalModel;
        expect(ModalModelState.show).toBe(true);
    });
    it('reducer: setText', () => {
        store.dispatch.ModalModel.setText(text);
        const ModalModelState = store.getState().ModalModel;
        expect(ModalModelState.text).toBe(text);
    });
});
